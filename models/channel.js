const mongoose = require('mongoose')

const channelSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Channel', channelSchema)