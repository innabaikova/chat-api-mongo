const express = require('express')
const router = express.Router()
const Message = require('../models/message')

// Get channel messages
router.get('/:id', async (req, res) => {
    try {
        let messages = await Message.find({ channelId: { $eq: req.params.id } })
        if (messages == null) {
            return res.status(404).json({
                message: `Cannot find messages for channel ${req.params.id}`
            })
        }
        res.json({ id: req.params.id, messages })
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
    
})

// Post message
router.post('/', async (req, res) => {
    const message = new Message(req.body)

    try {
        const newMessage = await message.save()
        res.status(201).json(newMessage)
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
})

module.exports = router
