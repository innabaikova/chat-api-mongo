const express = require('express')
const router = express.Router()
const Channel = require('../models/channel')

// Getting channels
router.get('/', async (req, res) => {
    try {
        const channels = await Channel.find()
        res.json(channels)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

// Create channel
router.post('/', async (req, res) => {
    const channel = new Channel({
        title: req.body.title
    })

    try {
        const newChannel = await channel.save()
        res.status(201).json(newChannel)    
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
})

module.exports = router
