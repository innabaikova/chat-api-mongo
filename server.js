require('dotenv').config()
const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')

mongoose.connect(process.env.DB_HOST, { useNewUrlParser: true })
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('Connected to database'))

app.use(cors())
app.use(express.json())

const channelsRouter = require('./routes/channels')
app.use('/channels', channelsRouter)

const messagesRouter = require('./routes/messages')
app.use('/messages', messagesRouter)

app.listen(4000, () => console.log('Server is running'))
